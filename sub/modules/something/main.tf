resource "random_id" "random_foo" {
  keepers = {
    life = 42
  }

  byte_length = 8
}

resource "local_file" "some" {
  content  = random_id.random_foo.hex
  filename = "/tmp/some"
}

